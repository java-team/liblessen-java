package com.metaweb.lessen;

import java.util.Properties;


public class Scope {
    final protected Scope _parent;
    final protected Properties _properties = new Properties();
    
    public Scope(Scope parent) {
        _parent = parent;
    }
    
    public void put(String name, Object obj) {
        _properties.put(name, obj);
    }
    
    public Object get(String name) {
        Object obj = _properties.get(name);
        if (obj == null && _parent != null) {
            obj = _parent.get(name);
        }
        return obj;
    }
}
