package com.metaweb.lessen.tests;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;

import com.metaweb.lessen.LessParser;
import com.metaweb.lessen.Utilities;
import com.metaweb.lessen.tokenizers.*;
import com.metaweb.lessen.tokens.Token;

public class TestCssPreserving {
    
    public static void main(String[] args) {
        try {
            testCssURL("http://52.global.apps.freebase.dev.freebaseapps.com/css-metaweb-global.css");
            testCssURL("http://freebaselibs.com/static/suggest/1.2.1/suggest.min.css");
            testCssURL("http://graphics8.nytimes.com/css/0.1/screen/build/homepage/styles.css");
            testCssURL("http://graphics8.nytimes.com/css/0.1/print/styles.css");
            //testCssURL("http://sc.wsj.net/djstyle/2/std/NA_WSJ/0_0_WH_0001_public-20100603200331.css");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static boolean testCssURL(String urlString) throws Exception {
        String input = loadFromURL(urlString);
        StringBuffer sb = new StringBuffer();
        
        Tokenizer tokenizer = new LessParser(Utilities.open(input), null);
        Token t;
        while ((t = tokenizer.getToken()) != null) {
            sb.append(t.text);
            tokenizer.next();
        }
        
        String output = sb.toString();
        
        boolean r = input.equals(output);
        if (r) {
            System.err.println("\n\n---------- Preserved " + urlString + " ----------");
        } else {
            System.err.println("\n\n----------  Error preserving " + urlString + " " + (output.length() - input.length()) + " ----------");
            
            String[] inputLines = input.split("\n");
            String[] outputLines = output.split("\n");
            int i;
            for (i = 0; i < inputLines.length && i < outputLines.length; i++) {
                String inputLine = inputLines[i];
                String outputLine = outputLines[i];
                if (!inputLine.equals(outputLine)) {
                    System.err.println("~" + i + ": " + outputLine);
                }
            }
            for (; i < inputLines.length ; i++) {
                String inputLine = inputLines[i];
                System.err.println("-: " + inputLine);
            }            
            for (; i < outputLines.length ; i++) {
                String outputLine = outputLines[i];
                System.err.println("+: " + outputLine);
            }            
            //System.out.println(output);
        }
        return r;
    }
    
    protected static String loadFromURL(String urlString) throws Exception {
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        String encoding = conn.getContentEncoding();
        
        Reader reader = encoding != null ?
            new InputStreamReader(conn.getInputStream(), encoding) :
            new InputStreamReader(conn.getInputStream());
            
        StringBuffer sb = new StringBuffer();
        try {
            char[] buffer = new char[4096];
            int c;
            
            while ((c = reader.read(buffer)) >= 0) {
                sb.append(buffer, 0, c);
            }
        } finally {
            reader.close();
        }
        return sb.toString();
    }
}
