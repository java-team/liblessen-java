package com.metaweb.lessen.tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.metaweb.lessen.Utilities;
import com.metaweb.lessen.tokenizers.*;

public class TestLessParsing {
    
    public static void main(String[] args) {
        try {
            testLess("tests/module-foo/dialog-x/dialog-x.css");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void testLess(String filePath) throws IOException {
        Map<String, String> variables = new HashMap<String, String>();
        variables.put("module", "foo");
        variables.put("theme", "midnight");
        
        Tokenizer tokenizer = Utilities.openLess(new File(filePath), variables);
        tokenizer = new UrlRewritingTokenizer(tokenizer, "../modules/foo/styles/");
        tokenizer = new CondensingTokenizer(tokenizer);
        tokenizer = new IndentingTokenizer(tokenizer);
        
        System.out.println("\n\n---------- Parsing LESS file " + filePath + " ----------");
        Utilities.print(tokenizer, System.out);
    }
}
