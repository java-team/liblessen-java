package com.metaweb.lessen.tests;

import java.net.URL;

import com.metaweb.lessen.Utilities;
import com.metaweb.lessen.tokenizers.*;

public class TestBeautifying {
    
    public static void main(String[] args) {
        try {
            beautify("http://52.global.apps.freebase.dev.freebaseapps.com/css-metaweb-global.css");
            beautify("http://freebaselibs.com/static/suggest/1.2.1/suggest.min.css");
            beautify("http://graphics8.nytimes.com/css/0.1/screen/build/homepage/styles.css");
            beautify("http://graphics8.nytimes.com/css/0.1/print/styles.css");
            beautify("http://x.myspacecdn.com/modules/common/static/css/global_--q9rjbg.css");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected static void beautify(String urlString) throws Exception {
        Tokenizer tokenizer = Utilities.open(new URL(urlString));
        tokenizer = new IndentingTokenizer(tokenizer);
        
        System.out.println("\n\n---------- Beautifying " + urlString + " ----------");
        Utilities.print(tokenizer, System.out);
    }
    
}
