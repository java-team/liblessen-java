package com.metaweb.lessen.expr;

import com.metaweb.lessen.Scope;
import com.metaweb.lessen.tokens.Token;

public interface Evaluable {
    public Token eval(Scope scope);
}
