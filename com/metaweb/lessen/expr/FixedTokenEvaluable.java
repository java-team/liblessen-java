package com.metaweb.lessen.expr;

import com.metaweb.lessen.Scope;
import com.metaweb.lessen.tokens.Token;


public class FixedTokenEvaluable implements Evaluable {
    final protected Token _token;
    
    public FixedTokenEvaluable(Token token) {
        _token = token;
    }

    @Override
    public Token eval(Scope scope) {
        return _token;
    }

}
