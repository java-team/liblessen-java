package com.metaweb.lessen.expr;

import java.util.List;

import com.metaweb.lessen.Scope;
import com.metaweb.lessen.tokens.Token;


public interface LessMixinFunction {
    public List<Token> invoke(List<Token> arguments, Scope scope);
}
