package com.metaweb.lessen.expr;

import com.metaweb.lessen.Scope;
import com.metaweb.lessen.tokens.Token;


public class VariableAccessEvaluable implements Evaluable {
    final protected Token _token;
    final protected String _name;
    
    public VariableAccessEvaluable(Token token, String name) {
        _token = token;
        _name = name;
    }
    
    @Override
    public Token eval(Scope scope) {
        Object obj = scope.get(_name);
        
        return (obj != null && obj instanceof Token) ? (Token) obj : _token;
    }
}
