package com.metaweb.lessen.tokens;

public class NumericWithUnitToken extends NumericToken {
    final public String unit;
    
    public NumericWithUnitToken(Type type, int start, int end, String text, Number n, String unit) {
        super(type, start, end, text, n);
        this.unit = unit;
    }

    @Override
    public String getCleanText() {
        return unit.equals("px") ? (n.intValue() + unit) : super.getCleanText();
    }
}
