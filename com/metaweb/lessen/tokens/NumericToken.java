package com.metaweb.lessen.tokens;

public class NumericToken extends Token {
    final public Number n;
    
    public NumericToken(Type type, int start, int end, String text, Number n) {
        super(type, start, end, text);
        this.n = n;
    }

}
