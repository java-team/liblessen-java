package com.metaweb.lessen.tokens;

public class UriToken extends StringValueToken {
    final public String prefix;
    
    public UriToken(int start, int end, String text, String prefix, String value) {
        super(Type.Uri, start, end, text, value);
        this.prefix = prefix;
    }
    
    @Override
    public String toString() {
        return typeToString(type) + ": " + prefix + "(" + unquotedText + ")";
    }
    
    @Override
    public String getCleanText() {
        return prefix + "(" + unquotedText + ")";
    }
}
