package com.metaweb.lessen.tokens;

import java.util.HashMap;
import java.util.Map;


public class OneTokenColor extends Color {
    protected int _r, _g, _b, _a;
    
    public OneTokenColor(int start, int end, String text, int r, int g, int b, int a) {
        super(start, end, text);
        _r = r; _g = g; _b = b; _a = a;
    }
    
    @Override
    public int getR() {
        return _r;
    }
    
    @Override
    public int getG() {
        return _g;
    }
    
    @Override
    public int getB() {
        return _b;
    }
    
    @Override
    public int getA() {
        return _a;
    }
    
    static protected int charToColorComponent(char c) {
        int n = charToNumber(c);
        return n * 16 + n;
    }
    
    static protected int twoCharsToColorComponent(String s) {
        return charToNumber(s.charAt(0)) * 16 + charToNumber(s.charAt(1));
    }
    
    static protected int charToNumber(char c) {
        int n = 0;
        if (c >= '0' && c <= '9') {
            n = c - '0';
        } else if (c >= 'a' && c <= 'f') {
            n = c - 'a';
        } else if (c >= 'A' && c <= 'F') {
            n = c - 'a';
        }
        return n;
    }
    
    static protected Map<String, int[]> s_colorMap;
    static {
        s_colorMap = new HashMap<String, int[]>();
        s_colorMap.put("AliceBlue".toLowerCase(), new int[] { 0xF0, 0xF8, 0xFF });
        s_colorMap.put("AntiqueWhite".toLowerCase(), new int[] { 0xFA, 0xEB, 0xD7 });
        s_colorMap.put("Aqua".toLowerCase(), new int[] { 0x00, 0xFF, 0xFF });
        s_colorMap.put("Aquamarine".toLowerCase(), new int[] { 0x7F, 0xFF, 0xD4 });
        s_colorMap.put("Azure".toLowerCase(), new int[] { 0xF0, 0xFF, 0xFF });
        s_colorMap.put("Beige".toLowerCase(), new int[] { 0xF5, 0xF5, 0xDC });
        s_colorMap.put("Bisque".toLowerCase(), new int[] { 0xFF, 0xE4, 0xC4 });
        s_colorMap.put("Black".toLowerCase(), new int[] { 0x00, 0x00, 0x00 });
        s_colorMap.put("BlanchedAlmond".toLowerCase(), new int[] { 0xFF, 0xEB, 0xCD });
        s_colorMap.put("Blue".toLowerCase(), new int[] { 0x00, 0x00, 0xFF });
        s_colorMap.put("BlueViolet".toLowerCase(), new int[] { 0x8A, 0x2B, 0xE2 });
        s_colorMap.put("Brown".toLowerCase(), new int[] { 0xA5, 0x2A, 0x2A });
        s_colorMap.put("BurlyWood".toLowerCase(), new int[] { 0xDE, 0xB8, 0x87 });
        s_colorMap.put("CadetBlue".toLowerCase(), new int[] { 0x5F, 0x9E, 0xA0 });
        s_colorMap.put("Chartreuse".toLowerCase(), new int[] { 0x7F, 0xFF, 0x00 });
        s_colorMap.put("Chocolate".toLowerCase(), new int[] { 0xD2, 0x69, 0x1E });
        s_colorMap.put("Coral".toLowerCase(), new int[] { 0xFF, 0x7F, 0x50 });
        s_colorMap.put("CornflowerBlue".toLowerCase(), new int[] { 0x64, 0x95, 0xED });
        s_colorMap.put("Cornsilk".toLowerCase(), new int[] { 0xFF, 0xF8, 0xDC });
        s_colorMap.put("Crimson".toLowerCase(), new int[] { 0xDC, 0x14, 0x3C });
        s_colorMap.put("Cyan".toLowerCase(), new int[] { 0x00, 0xFF, 0xFF });
        s_colorMap.put("DarkBlue".toLowerCase(), new int[] { 0x00, 0x00, 0x8B });
        s_colorMap.put("DarkCyan".toLowerCase(), new int[] { 0x00, 0x8B, 0x8B });
        s_colorMap.put("DarkGoldenRod".toLowerCase(), new int[] { 0xB8, 0x86, 0x0B });
        s_colorMap.put("DarkGray".toLowerCase(), new int[] { 0xA9, 0xA9, 0xA9 });
        s_colorMap.put("DarkGreen".toLowerCase(), new int[] { 0x00, 0x64, 0x00 });
        s_colorMap.put("DarkKhaki".toLowerCase(), new int[] { 0xBD, 0xB7, 0x6B });
        s_colorMap.put("DarkMagenta".toLowerCase(), new int[] { 0x8B, 0x00, 0x8B });
        s_colorMap.put("DarkOliveGreen".toLowerCase(), new int[] { 0x55, 0x6B, 0x2F });
        s_colorMap.put("Darkorange".toLowerCase(), new int[] { 0xFF, 0x8C, 0x00 });
        s_colorMap.put("DarkOrchid".toLowerCase(), new int[] { 0x99, 0x32, 0xCC });
        s_colorMap.put("DarkRed".toLowerCase(), new int[] { 0x8B, 0x00, 0x00 });
        s_colorMap.put("DarkSalmon".toLowerCase(), new int[] { 0xE9, 0x96, 0x7A });
        s_colorMap.put("DarkSeaGreen".toLowerCase(), new int[] { 0x8F, 0xBC, 0x8F });
        s_colorMap.put("DarkSlateBlue".toLowerCase(), new int[] { 0x48, 0x3D, 0x8B });
        s_colorMap.put("DarkSlateGray".toLowerCase(), new int[] { 0x2F, 0x4F, 0x4F });
        s_colorMap.put("DarkTurquoise".toLowerCase(), new int[] { 0x00, 0xCE, 0xD1 });
        s_colorMap.put("DarkViolet".toLowerCase(), new int[] { 0x94, 0x00, 0xD3 });
        s_colorMap.put("DeepPink".toLowerCase(), new int[] { 0xFF, 0x14, 0x93 });
        s_colorMap.put("DeepSkyBlue".toLowerCase(), new int[] { 0x00, 0xBF, 0xFF });
        s_colorMap.put("DimGray".toLowerCase(), new int[] { 0x69, 0x69, 0x69 });
        s_colorMap.put("DodgerBlue".toLowerCase(), new int[] { 0x1E, 0x90, 0xFF });
        s_colorMap.put("FireBrick".toLowerCase(), new int[] { 0xB2, 0x22, 0x22 });
        s_colorMap.put("FloralWhite".toLowerCase(), new int[] { 0xFF, 0xFA, 0xF0 });
        s_colorMap.put("ForestGreen".toLowerCase(), new int[] { 0x22, 0x8B, 0x22 });
        s_colorMap.put("Fuchsia".toLowerCase(), new int[] { 0xFF, 0x00, 0xFF });
        s_colorMap.put("Gainsboro".toLowerCase(), new int[] { 0xDC, 0xDC, 0xDC });
        s_colorMap.put("GhostWhite".toLowerCase(), new int[] { 0xF8, 0xF8, 0xFF });
        s_colorMap.put("Gold".toLowerCase(), new int[] { 0xFF, 0xD7, 0x00 });
        s_colorMap.put("GoldenRod".toLowerCase(), new int[] { 0xDA, 0xA5, 0x20 });
        s_colorMap.put("Gray".toLowerCase(), new int[] { 0x80, 0x80, 0x80 });
        s_colorMap.put("Green".toLowerCase(), new int[] { 0x00, 0x80, 0x00 });
        s_colorMap.put("GreenYellow".toLowerCase(), new int[] { 0xAD, 0xFF, 0x2F });
        s_colorMap.put("HoneyDew".toLowerCase(), new int[] { 0xF0, 0xFF, 0xF0 });
        s_colorMap.put("HotPink".toLowerCase(), new int[] { 0xFF, 0x69, 0xB4 });
        s_colorMap.put("IndianRed ".toLowerCase(), new int[] { 0xCD, 0x5C, 0x5C });
        s_colorMap.put("Indigo ".toLowerCase(), new int[] { 0x4B, 0x00, 0x82 });
        s_colorMap.put("Ivory".toLowerCase(), new int[] { 0xFF, 0xFF, 0xF0 });
        s_colorMap.put("Khaki".toLowerCase(), new int[] { 0xF0, 0xE6, 0x8C });
        s_colorMap.put("Lavender".toLowerCase(), new int[] { 0xE6, 0xE6, 0xFA });
        s_colorMap.put("LavenderBlush".toLowerCase(), new int[] { 0xFF, 0xF0, 0xF5 });
        s_colorMap.put("LawnGreen".toLowerCase(), new int[] { 0x7C, 0xFC, 0x00 });
        s_colorMap.put("LemonChiffon".toLowerCase(), new int[] { 0xFF, 0xFA, 0xCD });
        s_colorMap.put("LightBlue".toLowerCase(), new int[] { 0xAD, 0xD8, 0xE6 });
        s_colorMap.put("LightCoral".toLowerCase(), new int[] { 0xF0, 0x80, 0x80 });
        s_colorMap.put("LightCyan".toLowerCase(), new int[] { 0xE0, 0xFF, 0xFF });
        s_colorMap.put("LightGoldenRodYellow".toLowerCase(), new int[] { 0xFA, 0xFA, 0xD2 });
        s_colorMap.put("LightGrey".toLowerCase(), new int[] { 0xD3, 0xD3, 0xD3 });
        s_colorMap.put("LightGreen".toLowerCase(), new int[] { 0x90, 0xEE, 0x90 });
        s_colorMap.put("LightPink".toLowerCase(), new int[] { 0xFF, 0xB6, 0xC1 });
        s_colorMap.put("LightSalmon".toLowerCase(), new int[] { 0xFF, 0xA0, 0x7A });
        s_colorMap.put("LightSeaGreen".toLowerCase(), new int[] { 0x20, 0xB2, 0xAA });
        s_colorMap.put("LightSkyBlue".toLowerCase(), new int[] { 0x87, 0xCE, 0xFA });
        s_colorMap.put("LightSlateGray".toLowerCase(), new int[] { 0x77, 0x88, 0x99 });
        s_colorMap.put("LightSteelBlue".toLowerCase(), new int[] { 0xB0, 0xC4, 0xDE });
        s_colorMap.put("LightYellow".toLowerCase(), new int[] { 0xFF, 0xFF, 0xE0 });
        s_colorMap.put("Lime".toLowerCase(), new int[] { 0x00, 0xFF, 0x00 });
        s_colorMap.put("LimeGreen".toLowerCase(), new int[] { 0x32, 0xCD, 0x32 });
        s_colorMap.put("Linen".toLowerCase(), new int[] { 0xFA, 0xF0, 0xE6 });
        s_colorMap.put("Magenta".toLowerCase(), new int[] { 0xFF, 0x00, 0xFF });
        s_colorMap.put("Maroon".toLowerCase(), new int[] { 0x80, 0x00, 0x00 });
        s_colorMap.put("MediumAquaMarine".toLowerCase(), new int[] { 0x66, 0xCD, 0xAA });
        s_colorMap.put("MediumBlue".toLowerCase(), new int[] { 0x00, 0x00, 0xCD });
        s_colorMap.put("MediumOrchid".toLowerCase(), new int[] { 0xBA, 0x55, 0xD3 });
        s_colorMap.put("MediumPurple".toLowerCase(), new int[] { 0x93, 0x70, 0xD8 });
        s_colorMap.put("MediumSeaGreen".toLowerCase(), new int[] { 0x3C, 0xB3, 0x71 });
        s_colorMap.put("MediumSlateBlue".toLowerCase(), new int[] { 0x7B, 0x68, 0xEE });
        s_colorMap.put("MediumSpringGreen".toLowerCase(), new int[] { 0x00, 0xFA, 0x9A });
        s_colorMap.put("MediumTurquoise".toLowerCase(), new int[] { 0x48, 0xD1, 0xCC });
        s_colorMap.put("MediumVioletRed".toLowerCase(), new int[] { 0xC7, 0x15, 0x85 });
        s_colorMap.put("MidnightBlue".toLowerCase(), new int[] { 0x19, 0x19, 0x70 });
        s_colorMap.put("MintCream".toLowerCase(), new int[] { 0xF5, 0xFF, 0xFA });
        s_colorMap.put("MistyRose".toLowerCase(), new int[] { 0xFF, 0xE4, 0xE1 });
        s_colorMap.put("Moccasin".toLowerCase(), new int[] { 0xFF, 0xE4, 0xB5 });
        s_colorMap.put("NavajoWhite".toLowerCase(), new int[] { 0xFF, 0xDE, 0xAD });
        s_colorMap.put("Navy".toLowerCase(), new int[] { 0x00, 0x00, 0x80 });
        s_colorMap.put("OldLace".toLowerCase(), new int[] { 0xFD, 0xF5, 0xE6 });
        s_colorMap.put("Olive".toLowerCase(), new int[] { 0x80, 0x80, 0x00 });
        s_colorMap.put("OliveDrab".toLowerCase(), new int[] { 0x6B, 0x8E, 0x23 });
        s_colorMap.put("Orange".toLowerCase(), new int[] { 0xFF, 0xA5, 0x00 });
        s_colorMap.put("OrangeRed".toLowerCase(), new int[] { 0xFF, 0x45, 0x00 });
        s_colorMap.put("Orchid".toLowerCase(), new int[] { 0xDA, 0x70, 0xD6 });
        s_colorMap.put("PaleGoldenRod".toLowerCase(), new int[] { 0xEE, 0xE8, 0xAA });
        s_colorMap.put("PaleGreen".toLowerCase(), new int[] { 0x98, 0xFB, 0x98 });
        s_colorMap.put("PaleTurquoise".toLowerCase(), new int[] { 0xAF, 0xEE, 0xEE });
        s_colorMap.put("PaleVioletRed".toLowerCase(), new int[] { 0xD8, 0x70, 0x93 });
        s_colorMap.put("PapayaWhip".toLowerCase(), new int[] { 0xFF, 0xEF, 0xD5 });
        s_colorMap.put("PeachPuff".toLowerCase(), new int[] { 0xFF, 0xDA, 0xB9 });
        s_colorMap.put("Peru".toLowerCase(), new int[] { 0xCD, 0x85, 0x3F });
        s_colorMap.put("Pink".toLowerCase(), new int[] { 0xFF, 0xC0, 0xCB });
        s_colorMap.put("Plum".toLowerCase(), new int[] { 0xDD, 0xA0, 0xDD });
        s_colorMap.put("PowderBlue".toLowerCase(), new int[] { 0xB0, 0xE0, 0xE6 });
        s_colorMap.put("Purple".toLowerCase(), new int[] { 0x80, 0x00, 0x80 });
        s_colorMap.put("Red".toLowerCase(), new int[] { 0xFF, 0x00, 0x00 });
        s_colorMap.put("RosyBrown".toLowerCase(), new int[] { 0xBC, 0x8F, 0x8F });
        s_colorMap.put("RoyalBlue".toLowerCase(), new int[] { 0x41, 0x69, 0xE1 });
        s_colorMap.put("SaddleBrown".toLowerCase(), new int[] { 0x8B, 0x45, 0x13 });
        s_colorMap.put("Salmon".toLowerCase(), new int[] { 0xFA, 0x80, 0x72 });
        s_colorMap.put("SandyBrown".toLowerCase(), new int[] { 0xF4, 0xA4, 0x60 });
        s_colorMap.put("SeaGreen".toLowerCase(), new int[] { 0x2E, 0x8B, 0x57 });
        s_colorMap.put("SeaShell".toLowerCase(), new int[] { 0xFF, 0xF5, 0xEE });
        s_colorMap.put("Sienna".toLowerCase(), new int[] { 0xA0, 0x52, 0x2D });
        s_colorMap.put("Silver".toLowerCase(), new int[] { 0xC0, 0xC0, 0xC0 });
        s_colorMap.put("SkyBlue".toLowerCase(), new int[] { 0x87, 0xCE, 0xEB });
        s_colorMap.put("SlateBlue".toLowerCase(), new int[] { 0x6A, 0x5A, 0xCD });
        s_colorMap.put("SlateGray".toLowerCase(), new int[] { 0x70, 0x80, 0x90 });
        s_colorMap.put("Snow".toLowerCase(), new int[] { 0xFF, 0xFA, 0xFA });
        s_colorMap.put("SpringGreen".toLowerCase(), new int[] { 0x00, 0xFF, 0x7F });
        s_colorMap.put("SteelBlue".toLowerCase(), new int[] { 0x46, 0x82, 0xB4 });
        s_colorMap.put("Tan".toLowerCase(), new int[] { 0xD2, 0xB4, 0x8C });
        s_colorMap.put("Teal".toLowerCase(), new int[] { 0x00, 0x80, 0x80 });
        s_colorMap.put("Thistle".toLowerCase(), new int[] { 0xD8, 0xBF, 0xD8 });
        s_colorMap.put("Tomato".toLowerCase(), new int[] { 0xFF, 0x63, 0x47 });
        s_colorMap.put("Turquoise".toLowerCase(), new int[] { 0x40, 0xE0, 0xD0 });
        s_colorMap.put("Violet".toLowerCase(), new int[] { 0xEE, 0x82, 0xEE });
        s_colorMap.put("Wheat".toLowerCase(), new int[] { 0xF5, 0xDE, 0xB3 });
        s_colorMap.put("White".toLowerCase(), new int[] { 0xFF, 0xFF, 0xFF });
        s_colorMap.put("WhiteSmoke".toLowerCase(), new int[] { 0xF5, 0xF5, 0xF5 });
        s_colorMap.put("Yellow".toLowerCase(), new int[] { 0xFF, 0xFF, 0x00 });
        s_colorMap.put("YellowGreen".toLowerCase(), new int[] { 0x9A, 0xCD, 0x32 });
    }
    
    static public Color tokenToColor(Token token) {
        if (token.type == Type.Identifier) {
            int[] c = s_colorMap.get(token.text.toLowerCase());
            if (c != null) {
                return new OneTokenColor(token.start, token.end, token.text, c[0], c[1], c[2], -1);
            }
        } else if (token.type == Type.HashName) {
            String s = token.text;
            if (s.length() == 4 || s.length() == 5 || s.length() == 7 || s.length() == 9) {
                for (int i = 1; i < s.length(); i++) {
                    char c = s.charAt(i);
                    if (!Character.isDigit(c) &&
                        !(c >= 'a' && c <= 'f') &&
                        !(c >= 'A' && c <= 'F')) {
                        
                        return null;
                    }
                }
                
                s = s.substring(1);
                
                int r, g, b, a;
                if (s.length() < 6) {
                    r = charToColorComponent(s.charAt(0));
                    g = charToColorComponent(s.charAt(1));
                    b = charToColorComponent(s.charAt(2));
                    a = s.length() > 3 ? charToColorComponent(s.charAt(3)) : -1;
                } else {
                    r = twoCharsToColorComponent(s.substring(0, 2));
                    g = twoCharsToColorComponent(s.substring(2, 4));
                    b = twoCharsToColorComponent(s.substring(4, 6));
                    a = s.length() > 7 ? twoCharsToColorComponent(s.substring(6, 8)) : -1;
                }
                return new OneTokenColor(token.start, token.end, token.text, r, g, b, a);
            }
        }
        return null;
    }
}
