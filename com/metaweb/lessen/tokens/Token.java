package com.metaweb.lessen.tokens;


public class Token {
    public static enum Type {
        /*
         *  Mostly http://www.w3.org/TR/CSS2/syndata.html, with some changes
         */
        Invalid,
        
        Whitespace,
        Comment,
        Delimiter,      // { } [ ] ( ) : ;
        Operator,       // ~= |=
        
        CDataOpen,      // <!--
        CDataClose,     // -->
        
        Identifier,     // optionally prefixed with a dash
        AtIdentifier,   // possibly a LESS identifier
        HashName,       // such as color codes
        Function,       // such as expression(...)
        Variable,       // $name or ${name}, used for substitution
        
        String,
        Uri,
        
        Number,
        Percentage,
        Dimension,
        
        UnicodeRange,
        Color
    }
    
    static public String typeToString(Type type) {
        switch (type) {
        case Invalid: return "invalid";
        
        case Whitespace: return "whitespace";
        case Comment: return "comment";
        case Delimiter: return "delimiter";
        case Operator: return "operator";
        
        case CDataOpen: return "cdata-open";
        case CDataClose: return "cdata-close";
        
        case Identifier: return "identifier";
        case AtIdentifier: return "@identifier";
        case HashName: return "#name";
        case Function: return "function";
        case Variable: return "variable";
        
        case String: return "string";
        case Uri: return "uri";
        
        case Number: return "number";
        case Percentage: return "percentage";
        case Dimension: return "dimension";
        case UnicodeRange: return "unicode-range";
        case Color: return "color";
        }
        return "unknown";
    }
    
    final public Type   type;
    final public int    start;
    final public int    end;
    final public String text;
    
    public Token(Type type, int start, int end, String text) {
        this.type = type;
        this.start = start;
        this.end = end;
        this.text = text;
    }
    
    @Override
    public String toString() {
        if (type == Type.Whitespace) {
            return typeToString(type);
        } else {
            return typeToString(type) + ": " + text;
        }
    }
    
    public String getCleanText() {
        return text;
    }
}
