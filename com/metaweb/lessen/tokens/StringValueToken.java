package com.metaweb.lessen.tokens;

public class StringValueToken extends Token {
    final public String unquotedText;
    
    public StringValueToken(Type type, int start, int end, String text, String value) {
        super(type, start, end, text);
        this.unquotedText = value;
    }

    @Override
    public String toString() {
        return typeToString(type) + ": " + unquotedText;
    }
    
    @Override
    public String getCleanText() {
        return type == Type.Comment ? ("/*" + unquotedText + "*/") : text;
    }
}
