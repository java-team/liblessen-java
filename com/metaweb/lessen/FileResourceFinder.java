package com.metaweb.lessen;

import java.io.File;
import java.io.FileNotFoundException;

import com.metaweb.lessen.tokenizers.Tokenizer;


public class FileResourceFinder implements ResourceFinder {
    
    final protected File _dir;
    
    public FileResourceFinder(File file) {
        if (file.isDirectory()) {
            _dir = file;
        } else {
            _dir = file.getParentFile();
        }
    }

    @Override
    public Tokenizer open(String where) {
        File file = new File(_dir, where);
        
        try {
            return Utilities.open(file);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    @Override
    public ResourceFinder rebase(String where) {
        return new FileResourceFinder(new File(_dir, where));
    }

}
