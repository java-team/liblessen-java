package com.metaweb.lessen;

import java.util.ArrayList;
import java.util.List;

import com.metaweb.lessen.tokenizers.BufferedTokenizer;
import com.metaweb.lessen.tokenizers.Tokenizer;
import com.metaweb.lessen.tokens.Color;
import com.metaweb.lessen.tokens.Token;


public class LessParser extends ParserBase implements Tokenizer {
    final protected List<Token> _tokens = new ArrayList<Token>();
    
    static public boolean canBeIntermediateValue(Object o) {
        return o instanceof Token || o instanceof Color;
    }
    
    public LessParser(Tokenizer tokenizer, ResourceFinder resourceFinder) {
        this(tokenizer, resourceFinder, new Scope(null));
    }

    public LessParser(Tokenizer tokenizer, ResourceFinder resourceFinder, Scope scope) {
        super(new BufferedTokenizer(tokenizer), resourceFinder, scope, true);
    }

    @Override
    public Token getToken() {
        fill();
        return _tokens.size() > 0 ? _tokens.get(0) : null;
    }

    @Override
    public void next() {
        if (_tokens.size() > 0) {
            _tokens.remove(0);
        }
        fill();
    }
    
    protected void fill() {
        while (_tokens.size() == 0 && _tokenizer.getToken() != null) {
            parseStatement();
        }
    }

    @Override
    protected void passInnerBlockTokenThrough(Token t) {
        _tokens.add(t);
    }

    @Override
    protected void outputToken(Token t) {
        _tokens.add(t);
    }
}
