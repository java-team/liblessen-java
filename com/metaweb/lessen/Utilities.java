package com.metaweb.lessen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import com.metaweb.lessen.tokenizers.ReaderTokenizer;
import com.metaweb.lessen.tokenizers.RegroupingTokenizer;
import com.metaweb.lessen.tokenizers.StringTokenizer;
import com.metaweb.lessen.tokenizers.Tokenizer;
import com.metaweb.lessen.tokenizers.VariableResolvingTokenizer;
import com.metaweb.lessen.tokens.Token;


public class Utilities {

    static public Tokenizer open(String s) {
        return wrap(new StringTokenizer(s));
    }

    static public Tokenizer open(Reader reader) {
        return wrap(new ReaderTokenizer(reader));
    }
    
    static public Tokenizer open(File file) throws FileNotFoundException {
        return open(new FileReader(file));
    }
    
    static public Tokenizer open(URL url) throws IOException {
        URLConnection conn = url.openConnection();
        String encoding = conn.getContentEncoding();
        
        Reader reader = encoding != null ?
            new InputStreamReader(conn.getInputStream(), encoding) :
            new InputStreamReader(conn.getInputStream());
        
        return open(reader);
    }
    
    static public Tokenizer openLess(File file, Map<String, String> variables) throws FileNotFoundException {
        return openLess(file, variables, new Scope(null));
    }
    
    static public Tokenizer openLess(File file, Map<String, String> variables, Scope scope) throws FileNotFoundException {
        return wrapLess(open(file), new FileResourceFinder(file), variables, scope);
    }
    
    static public Tokenizer openLess(URL url, Map<String, String> variables) throws IOException {
        return openLess(url, variables, new Scope(null));
    }
    
    static public Tokenizer openLess(URL url, Map<String, String> variables, Scope scope) throws IOException {
        return wrapLess(open(url), new URLResourceFinder(url), variables, scope);
    }
    
    static public void write(Tokenizer tokenizer, Writer writer) throws IOException {
        Token t;
        while ((t = tokenizer.getToken()) != null) {
            writer.write(t.getCleanText());
            tokenizer.next();
        }
    }
    
    static public void print(Tokenizer tokenizer, PrintStream ps) throws IOException {
        Token t;
        while ((t = tokenizer.getToken()) != null) {
            ps.print(t.getCleanText());
            tokenizer.next();
        }
    }

    static protected Tokenizer wrap(Tokenizer t) {
        return new RegroupingTokenizer(t);
    }
    
    static protected Tokenizer wrapLess(Tokenizer tokenizer, ResourceFinder resourceFinder, Map<String, String> variables, Scope scope) {
        tokenizer = new LessParser(
            variables == null ? tokenizer : 
                new VariableResolvingTokenizer(tokenizer, variables, true),
            variables == null ? resourceFinder : 
                new VariableResolvingResourceFinder(resourceFinder, variables),
            scope);
        
        return tokenizer;
    }
}
