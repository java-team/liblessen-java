package com.metaweb.lessen;

import java.net.MalformedURLException;
import java.net.URL;

import com.metaweb.lessen.tokenizers.Tokenizer;


public class URLResourceFinder implements ResourceFinder {
    final protected URL _base;
    
    public URLResourceFinder(URL base) {
        _base = base;
    }

    @Override
    public Tokenizer open(String where) {
        try {
            URL url = new URL(_base, where);
            
            return Utilities.open(url);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ResourceFinder rebase(String where) {
        try {
            return new URLResourceFinder(new URL(_base, where));
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
