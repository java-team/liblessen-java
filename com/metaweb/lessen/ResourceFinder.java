package com.metaweb.lessen;

import com.metaweb.lessen.tokenizers.Tokenizer;

public interface ResourceFinder {
    public Tokenizer open(String where);
    
    public ResourceFinder rebase(String where);
}
