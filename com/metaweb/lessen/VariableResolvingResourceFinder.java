package com.metaweb.lessen;

import java.util.Map;

import com.metaweb.lessen.tokenizers.Tokenizer;
import com.metaweb.lessen.tokenizers.VariableResolvingTokenizer;

public class VariableResolvingResourceFinder implements ResourceFinder {
    final protected ResourceFinder _resourceFinder;
    final protected Map<String, String> _variables;
    
    public VariableResolvingResourceFinder(ResourceFinder resourceFinder, Map<String, String> variables) {
        _resourceFinder = resourceFinder;
        _variables = variables;
    }

    @Override
    public Tokenizer open(String where) {
        Tokenizer t = _resourceFinder.open(where);
        
        return t == null ? null : new VariableResolvingTokenizer(t, _variables);
    }
    
    @Override
    public ResourceFinder rebase(String where) {
        ResourceFinder rf = _resourceFinder.rebase(where);
        
        return rf == null ? null : new VariableResolvingResourceFinder(rf, _variables);
    }
}
