package com.metaweb.lessen.tokenizers;

import com.metaweb.lessen.tokens.Token;


public interface Tokenizer {
    public Token getToken();
    
    public void next();
}
