package com.metaweb.lessen.tokenizers;


public class StringTokenizer extends TokenizerBase {
    final protected String  _text;
    final protected int     _start;
    final protected int     _end;
    
    protected int           _next;

    public StringTokenizer(String text, int start, int end) {
        _text = text;
        _start = start;
        _end = end;
        _next = _start;
        
        next();
    }
    
    public StringTokenizer(String text) {
        this(text, 0, text.length());
    }
    

    @Override
    protected boolean hasMoreChar() {
        return _next < _end;
    }

    @Override
    protected void advance() {
        advance(1, false);
    }

    @Override
    protected void advance(int by, boolean flush) {
        _next += by;
        if (flush) {
            flush(_next);
        }
    }

    @Override
    protected void flush(int upToIndex) {
        // nothing to do in this case
    }

    @Override
    protected char getCharRelative(int offset) {
        return _text.charAt(_next + offset);
    }

    @Override
    protected char getCurrentChar() {
        return _text.charAt(_next);
    }

    @Override
    protected int getCurrentOffset() {
        return _next;
    }

    @Override
    protected String getText(int from, int to) {
        return _text.substring(from, to);
    }

    @Override
    protected boolean hasMoreChar(int offset) {
        return _next + offset < _end;
    }

}
