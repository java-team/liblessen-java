package com.metaweb.lessen.tokenizers;

import com.metaweb.lessen.tokens.Token;
import com.metaweb.lessen.tokens.Token.Type;


public class CondensingTokenizer implements Tokenizer {
    final protected BufferedTokenizer   _tokenizer;
    final protected boolean             _removeComments;
    protected Token                     _token;
    
    public CondensingTokenizer(Tokenizer tokenizer) {
        this(tokenizer, true);
    }
    
    public CondensingTokenizer(Tokenizer tokenizer, boolean removeComments) {
        _tokenizer = new BufferedTokenizer(tokenizer);
        _removeComments = removeComments;
        
        _token = _tokenizer.getToken();
        resolve();
    }
    
    @Override
    public Token getToken() {
        return _token;
    }

    @Override
    public void next() {
        _tokenizer.next();
        _token = _tokenizer.getToken();
        resolve();
    }
    
    protected void resolve() {
        while (_removeComments && _token != null && _token.type == Type.Comment) {
            _tokenizer.next();
            _token = _tokenizer.getToken();
        }
        
        if (_token != null && _token.type == Type.Whitespace) {
            StringBuffer sb = new StringBuffer();
            sb.append(_token.text);
            
            int lookahead = 1;
            Token t;
            while ((t = _tokenizer.getToken(lookahead)) != null && 
                    (t.type == Type.Whitespace || 
                     (_removeComments && t.type == Type.Comment))) {
                
                if (t.type == Type.Whitespace) {
                    sb.append(t.text);
                }
                lookahead++;
            }
            _tokenizer.next(lookahead - 1);
            
            _token = new Token(
                _token.type,
                _token.start,
                _token.end,
                sb.toString().indexOf('\n') < 0 ? " " : "\n"
            );
        }
    }
}
